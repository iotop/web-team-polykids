  var PolykidsData = 25;

    FusionCharts.ready(function () {
      var fusioncharts = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container',
        width: '100%',
        height: '300',
        dataFormat: 'json',
        dataSource: {
          "chart": {
            "caption": "Noise Sensor One",
            "subcaption": "Current Reading",
            "lowerLimit": "0",
            "upperLimit": "3",
            "theme": "fusion"
          },
          "colorRange": {
            "color": [{
              "minValue": "0",
              "maxValue": "1",
              "code": "#6baa01"
            }, {
              "minValue": "1",
              "maxValue": "2",
              "code": "#f8bd19"
            }, {
              "minValue": "2",
              "maxValue": "3",
              "code": "#e44a00"
            }]
          },
          "dials": {
            "dial": [{
              "value": "1"
            }]
          }

        },
        "events": {
          "initialized": function (evtObj, argObj) {
            evtObj.sender.changeInterval = setInterval(function () {
              evtObj.sender.feedData && evtObj.sender.feedData("value=" + senor1Data);
            }, 400);
          },
          "disposed": function (evtObj, argObj) {
            clearInterval(evtObj.sender.resetInterval);
            clearInterval(evtObj.sender.changeInterval);
          }
        }
      },

      );
      fusioncharts.render();
    });