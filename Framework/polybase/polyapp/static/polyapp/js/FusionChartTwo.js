
    FusionCharts.ready(function () {
      var fusioncharts = new FusionCharts({
        type: 'angulargauge',
        renderAt: 'chart-container2',
        width: '100%',
        height: '300',
        dataFormat: 'json',
        dataSource: {
          "chart": {
            "caption": "Noise Sensor Two",
            "subcaption": "Current Reading",
            "lowerLimit": "0",
            "upperLimit": "3",
            "theme": "fusion"
          },
          "colorRange": {
            "color": [{
              "minValue": "0",
              "maxValue": "1",
              "code": "#6baa01"
            }, {
              "minValue": "1",
              "maxValue": "2",
              "code": "#f8bd19"
            }, {
              "minValue": "2",
              "maxValue": "3",
              "code": "#e44a00"
            }]
          },
          "dials": {
            "dial": [{
              "value": "1"
            }]
          }

        },
        "events": {
          "initialized": function (evtObj, argObj) {
            evtObj.sender.changeInterval = setInterval(function () {
              evtObj.sender.feedData && evtObj.sender.feedData("value=" + senor2Data);
            }, 400);
          },
          "disposed": function (evtObj, argObj) {
            clearInterval(evtObj.sender.resetInterval);
            clearInterval(evtObj.sender.changeInterval);
          }
        }
      },

      );
      fusioncharts.render();
    });