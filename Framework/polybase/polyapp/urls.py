# app/urls.py

from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib import admin

from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('test/', views.test, name='test'),
    path('home/', views.home, name='home'),
    path('data/', views.data, name='data'),
    path('overview/', views.overview, name='overview'),
    path('blob/', views.blob, name='blob'),
    path('paho/', views.paho, name='paho')
]