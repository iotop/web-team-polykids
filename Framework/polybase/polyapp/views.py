# views.py

from django.shortcuts import render, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import auth
import requests
import json
import urllib.request
import datetime
from mongoengine import *
from pymongo import MongoClient
from bson import json_util
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
import paho.mqtt.client as mqtt
from django.http import JsonResponse
import datetime
import base64
import numpy as np


#Pymongo
def PymongoConnect():
    client = MongoClient('10.118.26.11', 27017)
    db = client.duniot_database
    db.authenticate('opiot', 'P@ssw0rd')
    mqtt_collection = db.node_data
    print("Connected")
    all_data = json_util.loads(json_util.dumps(mqtt_collection.find()))
    print(all_data)

#Isabel Pymongo
def PymongoConnectTwo():
    USER = 'opiot'
    PASS = 'P@ssw0rd'
    IP = '10.118.26.11'
    PORT = 27017

    user = urllib.parse.quote_plus(USER)
    passw = urllib.parse.quote_plus(PASS)
    client = MongoClient('mongodb://%s:%s@%s:%s' % (user, passw, IP, PORT))
    print("connected")
    db = client.duniot_database     # connect to the duniot_database
    mqtt_collection = db.node_data   # connect to the node_data collection
    #print(mqtt_collection)
    #print(db.node_data.find({"deviceName":"roomSensorOTAA"}))
    #node_data = json_util.loads(json_util.dumps(db.node_data.find("deviceName":"roomSensorOTAA")))
    #print(node_data)

    #data = json_util.loads(json_util.dumps(db.node_data.find({"deviceName":"senor2"}, {"deviceName", "applicationID", "dataEntries", "applicationName"})))
    #slicedData = json_util.loads(json_util.dumps(db.node_data.find({"deviceName":"sensor1"}, {"deviceName", "applicationID", "dataEntries", "applicationName"})))
    #data = json.loads(json.dumps(db.node_data.find({"deviceName":"roomSensorOTAA"}, {"deviceName", "applicationID", "dataEntries", "applicationName"})))
    #print(data[0]["dataEntries"][0])
    #print(data[0]["applicationName"])
    # print("dataEntries")
    #return(data)

    relevant_node_names = ["Gate1", "Gate2", "NoiseSenor1", "NioseSensor2", "room-sensor-D207", "room-sensor-D206b", "senor2", "sensor1", "sensor3"]
    relevant_nodes = {node_name:get_node_data(node_name, mqtt_collection) for node_name in relevant_node_names}
    return (relevant_nodes)

# convert base64 encoded string to bytes then to string
def base64_to_str(data):
    try:
        decoded = base64.b64decode(data).decode('utf-8')
    except UnicodeDecodeError:
        decoded = "Payload could not be decoded"
    return decoded


@api_view(['GET', 'POST'])
def paho(request):
   #broker = "iot.op-bit.nz"
   broker = "10.25.100.49"
   print("APIVIEWWORKING")
   if request.method =='GET':
        def on_message(client, userdata, message):
           print("received message =", str(message.payload.decode("utf-8")))
           payloadData = message.payload.decode("utf-8")
           jsonPayloadData = json_util.loads(json_util.dumps(payloadData))
           print("AAAAAAAAAAAAAAAAAAA" + jsonPayloadData["data"])
        def on_connect(client, userdata, flags, rc):
           print("Connected with result code" + str(rc))

        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        print("connecting to broker ", broker)
        client.connect(broker, 1883, 60)  # connect
        client.subscribe("#", 0)
        #client.username_pw_set("loraroot", "P@ssw0rd")
        client.loop_forever()
       # client.loop_start()  # start loop to process received messages
       #
       # client.disconnect()  # disconnect
       # client.loop_stop()  # stop loop

        return render(request, data)

def index(request):
    return HttpResponse('Hello World!')

def test(request):
	return render(request, 'polyapp/test.html')

def home(request):
	return render(request, 'polyapp/home.html')

def splitData(passedData, emptyData):
    cleanedData = []
    for i in passedData['dataEntries']:
        nodeData = base64.b64decode(i['data']).decode('utf-8')
        cutNodeData = nodeData[20:24]
        cleanedData.append(float(cutNodeData))
    cutCleanedData = list(chunks(cleanedData, 360))[-24:]
    for j in cutCleanedData:
        emptyData.append(round(np.mean(j), 2))

def data(request):
    sensor1 = PymongoConnectTwo()["sensor1"]
    sensor2 = PymongoConnectTwo()["senor2"]
    sensor3 = PymongoConnectTwo()["sensor3"]
    cleanedData = []
    senor1DataPerHour = []
    senor2DataPerHour = []
    senor3DataPerHour = []

    splitData(sensor1, senor1DataPerHour)
    splitData(sensor3, senor3DataPerHour)

    for i in sensor2['dataEntries']:
        nodeData = base64.b64decode(i['data']).decode('utf-8')
        cutNodeData = nodeData[20:24]
        cleanedData.append(float(cutNodeData))
    cutCleanedData = list(chunks(cleanedData, 360))[-24:]
    for j in cutCleanedData:
        senor2DataPerHour.append(round(np.mean(j), 2))
    sortedData = {
       'data': data,
       'senor1DataPerHour':senor1DataPerHour,
       'senor2DataPerHour':senor2DataPerHour,
       'senor3DataPerHour':senor3DataPerHour,
    }
    return render(request, 'polyapp/data.html', sortedData)

def get_most_recent_packet(mqtt_collection):
    pipeline = [
        {
            "$project": {
                "deviceName": 1,
                "most_recent": { "$arrayElemAt": ["$dataEntries", -1] }
            }
        }
    ]
    most_recent_data = mqtt_collection.aggregate(pipeline)
    return json_util.loads(json_util.dumps(most_recent_data))

# Get the name, application and data for one node in database by deviceName
# Return node as dictionary
def get_node_data(node_name, mqtt_collection):
    node = mqtt_collection.find({"deviceName":"%s" % node_name}, {"deviceName", "applicationID", "dataEntries", "applicationName"}).limit(1)
    if node:
        return json_util.loads(json_util.dumps(node))[0]
    else:
        return False

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i+n]

def overview(request):
   sensor2 = PymongoConnectTwo()["senor2"]
   cleanedData = []
   avgDataPerHour = []
   for i in sensor2['dataEntries']:
        nodeData = base64.b64decode(i['data']).decode('utf-8')
        cutNodeData = nodeData[20:24]
        cleanedData.append(float(cutNodeData))
   cutCleanedData = list(chunks(cleanedData, 360))[-24:]
   for j in cutCleanedData:
       avgDataPerHour.append(round(np.mean(j), 2))

   sortedData = {
       'data': data,
       'avgDataPerHour':avgDataPerHour
   }
   return render(request, 'polyapp/overview.html', sortedData)

def blob(request):
	return render(request, 'polyapp/blob.html')